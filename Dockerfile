FROM golang:1.19 as builder

WORKDIR /app
ADD . .

RUN go test ./... -v

RUN go build -o releaser

FROM ubuntu:22.10

RUN apt-get update -y && \
    apt-get install wget zip git -y

COPY --from=builder /app/releaser /usr/local/bin/releaser

RUN releaser --help