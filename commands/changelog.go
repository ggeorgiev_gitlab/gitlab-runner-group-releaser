package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var changelogCmd = &cobra.Command{
	Use:   "changelog [project name] [versions]...",
	Short: "Generates only the changelog for specified releases and writes them to stdout",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.Changelog()
	}),
}
