package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
	"os"
)

var rootCmd = &cobra.Command{
	Use:  "gitlab-runner-group-releaser",
	Args: cobra.MinimumNArgs(1),
}

type runWithReleaseCallback func(cmd *cobra.Command, args []string, release *release.Release) error

func RunWithRelease(callback runWithReleaseCallback) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("missing project name")
		}

		projectName := args[0]
		versions := args[1:]

		release, err := release.New(projectName, versions...)
		if err != nil {
			return err
		}

		if rootCmd.PersistentFlags().Lookup("dry-run").Value.String() == "true" {
			release.DryRun()
		}

		return callback(cmd, args, release)
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(changelogCmd)
	rootCmd.AddCommand(stableBranchCmd)
	rootCmd.AddCommand(tagCmd)
	rootCmd.AddCommand(waitTagCmd)
	rootCmd.AddCommand(waitStableBranchCmd)
	rootCmd.AddCommand(releaseGitlabCmd)

	rootCmd.PersistentFlags().Bool("dry-run", false, "Do not actually create any tags or branches, just print what would be done")
}
