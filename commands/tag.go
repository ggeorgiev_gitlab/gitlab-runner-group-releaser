package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var tagCmd = &cobra.Command{
	Use:   "tag [project name] [versions]...",
	Short: "Creates tags for specified releases",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.Tag()
	}),
}
