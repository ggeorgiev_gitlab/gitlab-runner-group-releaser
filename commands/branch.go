package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var stableBranchCmd = &cobra.Command{
	Use: "stable-branch [project name] [versions]...",
	Short: "Creates stable branches from the main branch or " +
		"from an already existing stable branch, depending on whether it's a patch release. Changelog is generated for the new release " +
		"and comited to the branch. Running this command multiple times will update the changelog correctly.",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.StableBranch()
	}),
}
