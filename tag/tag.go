package tag

import (
	"fmt"
	"golang.org/x/mod/semver"
	"strconv"
	"strings"
)

type Tag struct {
	major int
	minor int
	patch int
}

func Parse(tagRaw string) (Tag, error) {
	tag := strings.TrimSpace(tagRaw)
	if !semver.IsValid(tag) {
		return Tag{}, fmt.Errorf("tag is not a valid semver version: %s", tagRaw)
	}

	// remove leading v, we know it's there otherwise it wouldn't be a valid semver version
	segmentsSplit := strings.Split(tag[1:], ".")
	if len(segmentsSplit) < 3 {
		return Tag{}, fmt.Errorf("tag doesn't have enough segments, major, minor and patch segments are required: %s", tagRaw)
	}

	major, err := strconv.Atoi(segmentsSplit[0])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag major version %s: %s", tagRaw, segmentsSplit[0])
	}

	minor, err := strconv.Atoi(segmentsSplit[1])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag minor version %s: %s", tagRaw, segmentsSplit[1])
	}

	patch, err := strconv.Atoi(segmentsSplit[2])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag patch version %s: %s", tagRaw, segmentsSplit[2])
	}

	return Tag{
		major: major,
		minor: minor,
		patch: patch,
	}, nil
}

func (t *Tag) UnmarshalYAML(b []byte) error {
	tt, err := Parse(string(b))
	if err != nil {
		return err
	}

	*t = tt
	return nil
}

func (t Tag) String() string {
	return fmt.Sprintf("v%d.%d.%d", t.major, t.minor, t.patch)
}

func (t Tag) StringNoPrefix() string {
	return fmt.Sprintf("%d.%d.%d", t.major, t.minor, t.patch)
}

func (t Tag) AsStableBranch() string {
	return fmt.Sprintf("%d-%d-stable", t.major, t.minor)
}

func (t Tag) AsMinor() string {
	return fmt.Sprintf("%d.%d", t.major, t.minor)
}

func (t Tag) IsPatch() bool {
	return t.patch > 0
}

func (t Tag) Decrement() Tag {
	patch := t.patch
	minor := t.minor
	if t.IsPatch() {
		patch -= 1
	} else {
		minor -= 1
	}

	return Tag{
		major: t.major,
		minor: minor,
		patch: patch,
	}
}

func (t Tag) IsEmpty() bool {
	return t.major == 0 && t.minor == 0 && t.patch == 0
}
