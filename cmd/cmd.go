package cmd

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"os"
	"os/exec"
	"strings"
)

func cmd(wd, name string, out, err io.Writer, args ...string) *exec.Cmd {
	cmd := exec.Command(name, args...)

	cmd.Stdout = out
	cmd.Stderr = err
	cmd.Stdin = os.Stdin
	cmd.Dir = wd

	return cmd
}

type Cmd struct {
	cmd string
	wd  string
}

func (c *Cmd) log(args ...string) *zerolog.Logger {
	logger := log.With().
		Str("workdir", c.wd).
		Str("command", c.cmd).
		Strs("args", args).
		Logger()

	return &logger
}

func (c *Cmd) Wd(wd string) *Cmd {
	c.wd = wd
	return c
}

func (c *Cmd) Args(args ...string) WithArgs {
	return WithArgs{
		Cmd:  c,
		args: args,
	}
}

func (c *Cmd) ArgsInfallible(args ...string) WithArgs {
	return WithArgs{
		Cmd:        c,
		args:       args,
		infallible: true,
	}
}

func (c WithArgs) handleCmdErr(err error) error {
	if err == nil {
		return nil
	}

	if c.infallible {
		c.Cmd.log(c.args...).Warn().Err(err).Msg("A command failed, but we're ignoring it!")
		return nil
	}

	var exitError *exec.ExitError
	if errors.As(err, &exitError) {
		err = fmt.Errorf("%w: %s", err, strings.TrimSpace(string(exitError.Stderr)))
	}

	return fmt.Errorf("failed to run command %s [%s]: %w", c.cmd, c.args, err)
}

func (c WithArgs) Run() error {
	c.Cmd.log(c.args...).Info().Msg("Running command")
	err := cmd(c.wd, c.cmd, os.Stdout, os.Stderr, c.args...).Run()
	return c.handleCmdErr(err)
}

func (c WithArgs) RunOutput() (string, error) {
	out, err := cmd(c.wd, c.cmd, nil, nil, c.args...).Output()
	res := strings.TrimSpace(string(out))
	if err != nil {
		return res, c.handleCmdErr(err)
	}

	return res, nil
}

func New(c string) *Cmd {
	wd, _ := os.Getwd()
	return &Cmd{
		wd:  wd,
		cmd: c,
	}
}

type WithArgs struct {
	*Cmd
	args       []string
	infallible bool
}

type ManyWithArgs []WithArgs

func (c ManyWithArgs) Run() error {
	for _, cmd := range c {
		if err := cmd.Run(); err != nil {
			return err
		}
	}

	return nil
}
