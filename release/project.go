package release

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"gitlab-runner-group-releaser/cmd"
	"gitlab-runner-group-releaser/tag"
	"golang.org/x/mod/semver"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// TODO: I don't like how the env variables aren't listed in the --help output
const (
	EnvHttpsCloneUsername = "RELEASER_HTTPS_CLONE_USERNAME"
	EnvHttpsClonePassword = "RELEASER_HTTPS_CLONE_PASSWORD"
)

type Project struct {
	Name string `yaml:"name" json:"name"`
	ID   string `yaml:"id" json:"id"`
	URL  string `yaml:"url" json:"url"`

	mainBranch string
}

func (p Project) logger() *zerolog.Logger {
	logger := log.With().Stringer("project", p).Logger()
	return &logger
}

func (p Project) String() string {
	return fmt.Sprintf("project id: %s, name: %s", p.ID, p.Name)
}

func (p Project) Path() string {
	pwd, _ := os.Getwd()
	return strings.TrimSuffix(filepath.Join(pwd, ".tmp", p.Name), ".git")
}

func (p Project) Remote() string {
	return "origin"
}

func (p Project) MainBranch() string {
	return p.mainBranch
}

func (p Project) MainBranchRemote() string {
	return fmt.Sprintf("%s/%s", p.Remote(), p.MainBranch())
}

func (p Project) CloneURL() (string, error) {
	if strings.HasPrefix(p.URL, "https://") {
		u, err := url.Parse(p.URL)
		if err != nil {
			return "", err
		}

		u.Host = fmt.Sprintf("%s:%s@%s", os.Getenv(EnvHttpsCloneUsername), os.Getenv(EnvHttpsClonePassword), u.Host)
		url, _ := url.PathUnescape(u.String())
		return url, nil
	}

	return p.URL, nil
}

func (p Project) Prepare(versions []tag.Tag) error {
	p.logger().Info().Msg("Preparing project")

	cloneURL, err := p.CloneURL()
	if err != nil {
		return err
	}

	// TODO: remove all kinds of local resources that we should pull from the remote
	// TODO: we could probably pull less stuff
	dir := p.Path()
	if gitStatusErr := cmd.New("git").Wd(dir).Args("status").Run(); gitStatusErr != nil {
		p.logger().Info().Str("path", dir).Msg("Project path isn't a git repository, cloning project...")

		_ = os.RemoveAll(dir)

		parent := strings.TrimSuffix(dir, fmt.Sprintf("/%s", filepath.Base(dir)))

		if err := os.MkdirAll(parent, 0774); err != nil {
			return err
		}

		if err := cmd.New("git").Wd(parent).Args("clone", cloneURL, p.Name).Run(); err != nil {
			return err
		}
	}

	git := cmd.New("git").Wd(dir)

	// we need to manually set the remote for the cases where a project is already cloned from a specific URL,
	// but we change that URL. In this case the project will not be cloned again but the remote will be set to the
	// original
	if err := git.Args("remote", "rm", "origin").Run(); err != nil {
		var exitError *exec.ExitError
		if errors.As(err, &exitError) && exitError.ExitCode() != 2 {
			// exit status 2 is "No such remote"
			return fmt.Errorf("removing git remote %q %w", "origin", err)
		}
	}

	initRemoteCommands := cmd.ManyWithArgs{
		git.Args("remote", "add", "origin", cloneURL),
		git.Args("fetch", "origin"),
	}
	if err := initRemoteCommands.Run(); err != nil {
		return err
	}

	mainBranchOutput, err := git.Args("branch", "-r").RunOutput()
	if err != nil {
		return err
	}

	if strings.Contains(mainBranchOutput, fmt.Sprintf("%s/main", p.Remote())) {
		p.mainBranch = "main"
	} else if strings.Contains(mainBranchOutput, fmt.Sprintf("%s/master", p.Remote())) {
		p.mainBranch = "master"
	} else {
		return fmt.Errorf("could not find main or master branch in project %s", p.URL)
	}

	var commands cmd.ManyWithArgs

	commands = append(commands, git.Args("checkout", p.MainBranchRemote()))

	// remove all local branches and tags based on the ones we are going to release
	for _, v := range versions {
		commands = append(commands, git.ArgsInfallible("branch", "-D", v.AsStableBranch()))
		commands = append(commands, git.ArgsInfallible("tag", "-d", v.String()))
	}

	commands = append(commands, cmd.ManyWithArgs{
		git.Args("reset", "--hard", p.MainBranchRemote()),
		git.Args("checkout", p.MainBranchRemote()),
	}...)

	return commands.Run()
}

func (p Project) Tags() ([]tag.Tag, error) {
	git := cmd.New("git").Wd(p.Path())

	tagsRaw, err := git.Args("tags").RunOutput()
	if err != nil {
		return nil, err
	}

	tagsRawSplit := lo.Map(strings.Split(tagsRaw, ","), func(tag string, _ int) string {
		return strings.TrimSpace(tag)
	})

	for _, t := range tagsRawSplit {
		if !semver.IsValid(t) {
			return nil, fmt.Errorf("tag in project %s is not valid semver tag: %s", p.URL, t)
		}
	}

	semver.Sort(tagsRawSplit)

	return lo.Map(tagsRawSplit, func(t string, _ int) tag.Tag {
		tag, _ := tag.Parse(t)
		return tag
	}), nil
}
