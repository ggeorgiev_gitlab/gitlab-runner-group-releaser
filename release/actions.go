package release

import (
	"fmt"
	"gitlab-runner-group-releaser/cmd"
	changelogconfig "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/generator"
	"os"
	"path/filepath"
	"strings"
)

type Action interface {
	Execute(*ProjectConfig, *Release) error
}

type ChangelogEntryAction struct {
	Scope string `yaml:"scope" json:"scope"`
	Entry string `yaml:"entry" json:"entry"`
}

func (a *ChangelogEntryAction) Execute(c *ProjectConfig, _ *Release) error {
	c.AdditionalChangelogEntries = append(
		c.AdditionalChangelogEntries,
		generator.OptsAdditionalEntry{
			Scope: changelogconfig.Scope(a.Scope),
			Entry: a.Entry,
		},
	)

	return nil
}

type WriteFileAction struct {
	File     string `yaml:"file" json:"file"`
	Contents string `yaml:"contents" json:"contents"`
}

func (w *WriteFileAction) Execute(_ *ProjectConfig, release *Release) error {
	path := filepath.Join(release.Project.Path(), w.File)
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return fmt.Errorf("file %s does not exist but it must", path)
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC, 0)
	if err != nil {
		return err
	}

	// trim the contents and make sure there's always a newline at the end
	contents := strings.TrimSpace(w.Contents) + "\n"

	if _, err := f.WriteString(contents); err != nil {
		return err
	}

	return f.Close()
}

type CommitAction struct {
	Message string   `yaml:"message" json:"message"`
	Files   []string `yaml:"files" json:"files"`
}

func (c *CommitAction) Execute(_ *ProjectConfig, release *Release) error {
	git := release.ProjectGit()

	var commands cmd.ManyWithArgs
	for _, file := range c.Files {
		commands = append(commands, git.Args("add", file))
	}

	commands = append(commands, git.Args("commit", "-m", c.Message))

	return commands.Run()
}
