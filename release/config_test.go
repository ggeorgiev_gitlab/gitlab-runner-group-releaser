package release

import (
	"github.com/stretchr/testify/require"
	"os"
	"path/filepath"
	"testing"
)

func TestParseProjectConfig(t *testing.T) {
	wd, _ := os.Getwd()

	c, err := ParseProjectConfig(Context{
		Stage: ContextStage{
			Name: "changelog",
		},
		Release: ContextRelease{
			Version:    "15.8.2",
			AppVersion: "0.49.2",
		},
	}, ProjectConfigPathFromStage(filepath.Join(wd, "../test"), StageChangelog))
	require.NoError(t, err)
	require.NotNil(t, c)
}

func TestParseConfig(t *testing.T) {
	wd, _ := os.Getwd()

	c, err := LoadConfig(filepath.Join(wd, "../test/config.yml"))
	require.NoError(t, err)
	require.NotNil(t, c)
}
