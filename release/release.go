package release

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"github.com/sourcegraph/conc/iter"
	"github.com/xanzy/go-gitlab"
	"gitlab-runner-group-releaser/client"
	"gitlab-runner-group-releaser/cmd"
	"gitlab-runner-group-releaser/tag"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/extractor"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/generator"
	gitchangelog "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
	"golang.org/x/sync/errgroup"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

const (
	EnvReleaseGitLabToken = "RELEASER_GITLAB_TOKEN"
	EnvConfigProjectPath  = "RELEASER_CONFIG_PROJECT_PATH"

	TriggerGitLabReleaseJobName = "stable gitlab release"
)

type Release struct {
	Project Project

	configProject  Project
	releasesConfig ReleasesConfig
	config         *Config
	gitlab         *client.GitLab

	dryRun bool
}

func New(name string, versions ...string) (*Release, error) {
	cfg, err := LoadConfig(os.Getenv(EnvConfigProjectPath))
	if err != nil {
		return nil, err
	}

	releasesConfig := lo.Filter(cfg.Releases, func(r ReleaseConfig, _ int) bool {
		if r.Name == name && len(versions) == 0 {
			return true
		}

		return r.Name == name && lo.Contains(versions, r.Version.String())
	})
	if len(releasesConfig) == 0 {
		return nil, fmt.Errorf("release %q not found in config", name)
	}

	proj, found := lo.Find(cfg.Projects, func(p Project) bool {
		return p.Name == name
	})
	if !found {
		return nil, fmt.Errorf("proj %q not found in config", name)
	}

	gitlabClient, err := client.NewGitLab(os.Getenv(EnvReleaseGitLabToken), proj.ID)
	if err != nil {
		return nil, err
	}

	release := &Release{
		Project:        proj,
		releasesConfig: releasesConfig,

		config: cfg,
		gitlab: gitlabClient,
	}

	sort.Sort(release.releasesConfig)

	return release, nil
}

func (r *Release) DryRun() {
	r.dryRun = true
}

func (r *Release) runProjectConfig(stage Stage, releaseConfig ReleaseConfig) (*ProjectConfig, error) {
	cfg, err := ParseProjectConfig(Context{
		Stage: ContextStage{
			Name: stage,
		},
		Release: ContextRelease{
			Version:    releaseConfig.Version.StringNoPrefix(),
			AppVersion: releaseConfig.AppVersion.StringNoPrefix(),
		},
	}, ProjectConfigPathFromStage(r.Project.Path(), stage))
	if err != nil {
		return nil, err
	}

	if err := cfg.Execute(r); err != nil {
		return nil, err
	}

	return cfg, nil
}

func (r *Release) log() *zerolog.Logger {
	logger := log.With().Str("project", r.Project.ID).Logger()
	return &logger
}

func (r *Release) logWithTag(tag tag.Tag) *zerolog.Logger {
	logger := r.log().With().Stringer("tag", tag).Logger()
	return &logger
}

func (r *Release) Tag() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()); err != nil {
		return err
	}

	for _, cfg := range r.releasesConfig {
		if err := r.createTag(cfg); err != nil {
			return err
		}
	}

	return nil
	// # For each tag
	// * Important * Tag must be sorted and created in order from oldest to newest so they appear in the correct order in the /tags page of the Project
	// All the checks can be done sequentially or out of order but the final tag creation must be done in order

	//1. git checkout main && git pull && git checkout -b 15-7-stable
	//2. make generate_changelog CHANGELOG_RELEASE=v15.7.0
	//3. Go through each changelog MR entry and verify that:
	//	3.1 Milestone is correctly set to current milestone. If this is a patch release, e.g. 15.6.1 the milestone still needs to be the latest patch - %15.7
	//	3.2 Correct labels are set: maintentance / feature / docs etc. Just check for their existance no need for further and deeper checks
	// 	3.3 Check the title doesn't contain prefixes - feat:, docs:
	// 	3.4 Check the title is properly capitalized so the final changelog is uniform
	//	3.5 Try to collect as much info as possible, avoid failing fast
	//4. git add CHANGELOG.md && git commit -m "Update CHANGELOG for v15.7.0"
	//5. git tag v15.7.0 -m "RunnerVersion v15.7.0" && git push origin v15.7.0 15-7-stable

	// Notes:
	// All the steps above should not break the workflow if they are ran twice. Changelog should check if it's generated, tag should be checked if it's created etc.

	// TODO: maybe generate release checklist as part of the pipeline

	// TODO: do this in a final step

	// Finally:
	// Each tag should be merged into main
	// ** ONLY ** if we are doing a normal .0 release - the main branch needs to have its version bumped to 15.8.0
	// The command bellow needs to be split into segments

	// git checkout main && \
	// git pull && \
	// git merge --no-ff 15-7-stable && \
	// echo -n "15.8.0" > VERSION && \
	// git add VERSION && \
	// git commit -m "Bump version to 15.8.0" && \
	// git push
}

func (r *Release) Changelog() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()); err != nil {
		return err
	}

	// we have already guaranteed that there is at least one tag during initialization
	for _, cfg := range r.releasesConfig {
		t := cfg.Version

		stableBranch := t.AsStableBranch()

		if r.gitBranchExistsRemotely(stableBranch) {
			// we are already on the default branch, checkout to stable only if needed
			if err := r.checkoutStableBranch(t); err != nil {
				return err
			}
		}

		projectConfig, err := r.runProjectConfig(StageChangelog, cfg)
		if err != nil {
			return err
		}

		if err := r.generateChangelog(cfg, projectConfig); err != nil {
			return err
		}

		_ = r.diffChangelogCommand().Run()
	}

	return nil
}

func (r *Release) checkoutStableBranch(t tag.Tag) error {
	return r.ProjectGit().Args("checkout", fmt.Sprintf("%s/%s", r.Project.Remote(), t.AsStableBranch())).Run()
}

func (r *Release) checkoutNewStableBranch(t tag.Tag) error {
	return r.ProjectGit().Args("checkout", "-b", t.AsStableBranch()).Run()
}

func (r *Release) mustCheckoutStableBranch(t tag.Tag) error {
	if r.gitBranchExistsRemotely(t.AsStableBranch()) {
		return r.checkoutStableBranch(t)
	}

	return r.checkoutNewStableBranch(t)
}

func (r *Release) StableBranch() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()); err != nil {
		return err
	}

	for _, cfg := range r.releasesConfig {
		if err := r.createStableBranch(cfg); err != nil {
			return err
		}
	}

	return nil
}

func (r *Release) generateChangelog(cfg ReleaseConfig, c *ProjectConfig) error {
	t := cfg.Version
	mrIIDs, err := r.processReleaseMergeRequests(cfg)
	if err != nil {
		return err
	}

	if len(mrIIDs)+len(c.AdditionalChangelogEntries) == 0 {
		r.logWithTag(t).Info().Msg("No additional changelog entries found either, skipping changelog generation")
		return nil
	}

	opts := generator.Opts{
		ProjectID:         r.Project.ID,
		Release:           t.String(),
		StartingPoint:     t.Decrement().String(),
		WorkingDirectory:  r.Project.Path(),
		ChangelogFile:     filepath.Join(r.Project.Path(), "CHANGELOG.md"),
		ConfigFile:        filepath.Join(r.Project.Path(), ".gitlab", "changelog.yml"),
		MrIIDs:            mrIIDs,
		AdditionalEntries: c.AdditionalChangelogEntries,
		OverwriteRelease:  true,
	}

	g, err := generator.New(opts)
	if err != nil {
		return fmt.Errorf("creating changelog generator %w", err)
	}

	if err := g.Generate(); err != nil {
		return fmt.Errorf("generating changelog %w", err)
	}

	return nil
}

func (r *Release) ProjectGit() *cmd.Cmd {
	return cmd.New("git").Wd(r.Project.Path())
}

func (r *Release) gitBranchExistsRemotely(branch string) bool {
	return r.ProjectGit().Args("ls-remote", "--exit-code", "--heads", r.Project.Remote(), branch).Run() == nil
}

func (r *Release) gitBranchExistsLocally(branch string) bool {
	return r.ProjectGit().Args("rev-parse", "--verify", branch).Run() == nil
}

func (r *Release) diffChangelogCommand() cmd.WithArgs {
	return r.ProjectGit().Args("diff", "--exit-code", "CHANGELOG.md")
}

// createStableBranch creates a stable branch for the given tag
// and generates a changelog for it. The changelog will be regenerated accordingly
// if the function is run more than once on an existing branch and changelog
func (r *Release) createStableBranch(releaseConfig ReleaseConfig) error {
	t := releaseConfig.Version

	stableBranch := t.AsStableBranch()

	r.logWithTag(t).
		Info().
		Str("stable_branch", stableBranch).
		Msg("Creating stable branch")

	git := r.ProjectGit()

	if err := r.mustCheckoutStableBranch(t); err != nil {
		return err
	}

	cfg, err := r.runProjectConfig(StageStableBranch, releaseConfig)
	if err != nil {
		return err
	}

	if err := r.generateChangelog(releaseConfig, cfg); err != nil {
		return err
	}

	var commands cmd.ManyWithArgs
	if err := r.diffChangelogCommand().Run(); err != nil {
		commands = append(commands, git.Args("add", "CHANGELOG.md"))
		commands = append(commands, git.Args("commit", "-m", fmt.Sprintf("Update CHANGELOG for %s", t.String())))
	}

	if !r.dryRun {
		commands = append(commands, git.Args("push", r.Project.Remote(), fmt.Sprintf("HEAD:%s", stableBranch)))
	} else {
		log.Warn().Msg("Skipping push to remote because of dry run")
	}

	return commands.Run()
}

func (r *Release) createTag(releaseConfig ReleaseConfig) error {
	t := releaseConfig.Version

	log.Info().Stringer("tag", t).Msg("Creating tag")

	if err := r.checkoutStableBranch(t); err != nil {
		return err
	}

	_, err := r.runProjectConfig(StageTag, releaseConfig)
	if err != nil {
		return err
	}

	git := r.ProjectGit()
	commands := cmd.ManyWithArgs{
		git.Args("tag", t.String(), "-m", fmt.Sprintf("Version %s", t.String())),
	}

	if !r.dryRun {
		commands = append(commands, git.Args("push", r.Project.Remote(), t.String()))
	} else {
		log.Warn().Msg("Skipping push to remote because of dry run")
	}

	return commands.Run()
}

func (r *Release) processReleaseMergeRequests(cfg ReleaseConfig) ([]int, error) {
	t := cfg.Version
	r.logWithTag(t).Info().Msg("Extracting release merge requests")

	mrIIDs, err := extractor.New(gitchangelog.New(r.Project.Path())).
		ExtractMRIIDs(t.Decrement().String(), "")
	if err != nil {
		return nil, err
	}

	if len(mrIIDs) == 0 {
		r.logWithTag(t).Info().Msg("No merge requests found for this release")
		return nil, nil
	}

	r.logWithTag(t).Info().Ints("mr_iids", mrIIDs).Msg("Extracted release merge requests")

	var milestone *gitlab.Milestone
	if !t.IsPatch() {
		r.logWithTag(t).Info().Msg("Listing milestones")
		milestones, _, err := r.gitlab.Milestones.ListMilestones(r.Project.ID, &gitlab.ListMilestonesOptions{
			IncludeParentMilestones: gitlab.Bool(true),
			Search:                  gitlab.String(cfg.Milestone().AsMinor()),
		})
		if err != nil {
			return nil, fmt.Errorf("listing milestones: %w", err)
		}

		milestone, _ = lo.Find(milestones, func(m *gitlab.Milestone) bool {
			return m.Title == cfg.Milestone().AsMinor()
		})
		if milestone == nil {
			return nil, fmt.Errorf("milestone %s not found", t.AsMinor())
		}
	}

	iterator := iter.Mapper[int, int]{
		MaxGoroutines: 10,
	}

	r.logWithTag(t).Info().Msg("Processing merge requests")
	return iterator.MapErr(mrIIDs, func(mrIID *int) (int, error) {
		iid := *mrIID

		r.logWithTag(t).Info().Int("mr_iid", iid).Msg("Processing merge request")
		mr, _, err := r.gitlab.MergeRequests.GetMergeRequest(r.Project.ID, iid, nil)
		if err != nil {
			return 0, fmt.Errorf("getting merge request %d: %w", iid, err)
		}

		updateOpts := &gitlab.UpdateMergeRequestOptions{}

		updateOpts.MilestoneID, err = r.reconcileMergeRequestMilestoneID(mr, milestone, cfg.Milestone())
		if err != nil {
			return 0, fmt.Errorf("reconciling merge request milestone: %w", err)
		}

		updateOpts.Title = r.reconcileMergeRequestTitle(mr, t)

		if updateOpts.MilestoneID != nil || updateOpts.Title != nil {
			if !r.dryRun {
				_, _, err = r.gitlab.MergeRequests.UpdateMergeRequest(r.Project.ID, iid, updateOpts)
				if err != nil {
					return 0, fmt.Errorf("updating merge request %d: %w", iid, err)
				}
			} else {
				r.logWithTag(t).Warn().Int("mr_iid", iid).Msg("Skipping merge request update because of dry run")
			}
		}

		if err := r.assertMergeRequestLabels(mr, t); err != nil {
			return 0, fmt.Errorf("asserting merge request labels: %w", err)
		}

		return iid, nil
	})
}

func (r *Release) reconcileMergeRequestMilestoneID(mr *gitlab.MergeRequest, milestone *gitlab.Milestone, t tag.Tag) (*int, error) {
	if milestone == nil {
		return nil, nil
	}

	if mr.Milestone != nil && mr.Milestone.Title == t.AsMinor() {
		r.logWithTag(t).Info().Int("mr_iid", mr.IID).Msg("Milestone is already correctly set")
		return &mr.Milestone.ID, nil
	}

	r.logWithTag(t).Info().Int("mr_iid", mr.IID).Str("new_milestone", milestone.Title).Msg("Will update milestone")
	return &milestone.ID, nil
}

func (r *Release) reconcileMergeRequestTitle(mr *gitlab.MergeRequest, t tag.Tag) *string {
	// Add more prefixes here
	prefixes := []string{
		"feat:",
		"docs:",
		"doc:",
		"bug:",
		"fix:",
	}

	r.logWithTag(t).Info().Int("md_iid", mr.IID).Str("title", mr.Title).Msg("Processing title")

	title := mr.Title
	titleLower := strings.ToLower(mr.Title)
	for _, prefix := range prefixes {
		if strings.HasPrefix(titleLower, prefix) {
			title = strings.TrimSpace(title[len(prefix):])
			r.logWithTag(t).Info().
				Int("mr_iid", mr.IID).
				Str("new_title", title).
				Str("prefix", prefix).
				Msg("Removed prefix")

			break
		}
	}

	title = strings.ToUpper(title[0:1]) + title[1:]
	if title != mr.Title {
		r.logWithTag(t).Info().
			Int("mr_iid", mr.IID).
			Str("new_title", title).
			Msg("Capitalized title")

		return &title
	}

	return nil
}

func (r *Release) assertMergeRequestLabels(mr *gitlab.MergeRequest, t tag.Tag) error {
	r.logWithTag(t).Info().Int("mr_iid", mr.IID).Msg("Asserting merge request labels")

	labelPrefixes := []string{
		"type::",
	}

	for _, labelPrefix := range labelPrefixes {
		label, found := lo.Find(mr.Labels, func(l string) bool {
			return strings.HasPrefix(l, labelPrefix)
		})
		if !found {
			return fmt.Errorf("label with prefix %q not found for merge request !%d (%s)", labelPrefix, mr.IID, mr.Title)
		}

		r.logWithTag(t).Info().Int("mr_iid", mr.IID).Str("label", label).Msg("Found label")
	}

	return nil
}

func (r *Release) getTagPipeline(tag tag.Tag) (*gitlab.PipelineInfo, error) {
	r.logWithTag(tag).Info().Msg("Getting tag pipeline")

	gitlabTag, _, err := r.gitlab.Tags.GetTag(r.Project.ID, tag.String())
	if err != nil {
		return nil, fmt.Errorf("getting tag %s: %w", tag, err)
	}

	// we need to get the commit as the pipeline is not populated in the tag above
	commit, _, err := r.gitlab.Commits.GetCommit(r.Project.ID, gitlabTag.Commit.ID)
	if err != nil {
		return nil, fmt.Errorf("getting commit %s: %w", gitlabTag.Commit.ID, err)
	}

	return commit.LastPipeline, nil
}

func (r *Release) getBranchPipeline(tag tag.Tag) (*gitlab.PipelineInfo, error) {
	r.logWithTag(tag).Info().Msg("Getting branch pipeline")

	branch, _, err := r.gitlab.Branches.GetBranch(r.Project.ID, tag.AsStableBranch())
	if err != nil {
		return nil, fmt.Errorf("getting stable branch %s: %w", tag, err)
	}

	// we need to get the commit as the pipeline is not populated in the tag above
	commit, _, err := r.gitlab.Commits.GetCommit(r.Project.ID, branch.Commit.ID)
	if err != nil {
		return nil, fmt.Errorf("getting commit %s: %w", branch.Commit.ID, err)
	}

	return commit.LastPipeline, nil
}

func (r *Release) WaitTag() error {
	var wg errgroup.Group
	for _, cfg := range r.releasesConfig {
		pipeline, err := r.getTagPipeline(cfg.Version)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		wg.Go(func() error {
			return r.waitPipelineFinish(pipeline.ID, gitlab.Success)
		})
	}

	return wg.Wait()
}

func (r *Release) WaitStableBranch() error {
	var wg errgroup.Group
	for _, cfg := range r.releasesConfig {
		pipeline, err := r.getBranchPipeline(cfg.Version)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		wg.Go(func() error {
			return r.waitPipelineFinish(pipeline.ID, gitlab.Success)
		})
	}

	return wg.Wait()
}

func (r *Release) waitPipelineFinish(pipelineID int, desiredSuccessStatus gitlab.BuildStateValue) error {
	var logger = func() *zerolog.Logger {
		logger := r.log().With().Int("pipeline_id", pipelineID).Logger()
		return &logger
	}

	var i int
	for {
		if i > 0 {
			timeout := 10 * time.Second
			logger().Info().
				Dur("timeout", timeout).
				Msg("Waiting until next pipeline check...")

			time.Sleep(timeout)
		}
		i++

		finished, status, err := r.checkPipelineStatus(pipelineID, desiredSuccessStatus)
		if finished {
			logger().Info().Str("status", string(status)).Msg("Pipeline finished")
			return err
		}

		if err != nil {
			logger().Warn().Err(err).Msg("waiting pipeline")
		}
	}
}

func (r *Release) checkPipelineStatus(pipelineID int, desiredSuccessStatus gitlab.BuildStateValue) (bool, gitlab.BuildStateValue, error) {
	pipeline, _, err := r.gitlab.Pipelines.GetPipeline(r.Project.ID, pipelineID)
	if err != nil {
		return false, "", err
	}

	status := pipeline.Status
	r.log().Info().Int("pipeline_id", pipelineID).Str("status", status).Msg("Pipeline status")
	switch gitlab.BuildStateValue(status) {
	case desiredSuccessStatus:
		return true, gitlab.BuildStateValue(status), nil
	case gitlab.Failed, gitlab.Canceled, gitlab.Skipped:
		return true, gitlab.BuildStateValue(status), fmt.Errorf("pipeline didn't pass, status: %s", status)
	default:
		return false, "", nil
	}
}

func (r *Release) RunGitlabRelease() error {
	for _, cfg := range r.releasesConfig {
		t := cfg.Version

		r.logWithTag(t).Info().Msg("Running gitlab release")

		pipeline, err := r.getTagPipeline(t)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		r.logWithTag(t).Info().Msg("Listing pipeline jobs")
		jobs, _, err := r.gitlab.Jobs.ListPipelineJobs(r.Project.ID, pipeline.ID, &gitlab.ListJobsOptions{
			IncludeRetried: gitlab.Bool(true),
		})
		if err != nil {
			return fmt.Errorf("getting pipeline jobs: %w", err)
		}

		releaseJob, _ := lo.Find(jobs, func(j *gitlab.Job) bool {
			return j.Name == TriggerGitLabReleaseJobName
		})
		if releaseJob == nil {
			jobsNames := lo.Map(jobs, func(j *gitlab.Job, _ int) string {
				return j.Name
			})
			r.logWithTag(t).Warn().Strs("jobs", jobsNames).Msg("Project has no release job...skipping")
			// TODO: mark the CI job as able to fail, this way we'll be able to release even if the release job is not present
			return fmt.Errorf("project has no release job")
		}

		r.logWithTag(t).Info().Str("status", releaseJob.Status).Msg("Release job status")

		switch gitlab.BuildStateValue(releaseJob.Status) {
		case gitlab.Manual:
			r.logWithTag(t).Info().Msg("Playing manual release job")
			_, _, err = r.gitlab.Jobs.PlayJob(r.Project.ID, releaseJob.ID, nil)
			if err != nil {
				return fmt.Errorf("playing release job: %w", err)
			}
		case gitlab.Success, gitlab.Failed, gitlab.Canceled:
			r.logWithTag(t).Info().Msg("Retrying release job")
			_, _, err = r.gitlab.Jobs.RetryJob(r.Project.ID, releaseJob.ID, nil)
			if err != nil {
				return fmt.Errorf("restarting release job: %w", err)
			}
		default:
			r.logWithTag(t).Warn().Msg("Skipping staring release job as it's already in an acceptable status")
		}

		if err := r.waitPipelineFinish(pipeline.ID, gitlab.Success); err != nil {
			return fmt.Errorf("wait gitlab release pipeline finish: %w", err)
		}
	}

	return nil
}
